<?php

// Класс для главы 6 (3-6)
class Student1
{
    public $name;
    public $course;

    // Метод для перевода на следующий курс:
    public function transferToNextCourse($course)
    {
        // Проверим курс на корректность:
        if ($this->isCourseCorrect($course)) {
            $this->course = $course + 1;
        }
        else $this->course = "Достигнут 5 курс";
    }

    // ПРИВАТНЫЙ метод для проверки курса:
    private function isCourseCorrect($course)
    {
        if ($course > 0 and $course < 5) {
            return true;
        } else {
            return false;
        }
    }
}
