<?php

// Класс для главы 4 (1-6)
class Employee2
{
    public $name;
    public $age;
    public $salary;

    public function getName()
    {
        // Возвращаем имя:
        return $this->name;
    }

    public function getAge()
    {
        // Возвращаем возраст:
        return $this->age;
    }

    public function getSalary()
    {
        // Возвращаем зарплату:
        return $this->salary;
    }

    public function checkAge()
    {
        // Проверка возраста >18:
        if ($this->age > 18) {
            echo 'true';
        }
        else echo 'false';
        ;
    }
}