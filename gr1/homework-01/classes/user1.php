<?php

// Класс для главы 4 (7-10)
class User1
{
    public $name;
    public $age;

    // Метод для изменения возраста юзера:
    public function setAge($age) {
        if ($age >= 18) {
            $this->age = $age;
        }
    }
}
