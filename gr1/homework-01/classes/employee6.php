<?php

// Класс для главы 9
class Employee6
{
    private $name;
    private $surname;
    private $salary;

    // Конструктор объекта:
    public function __construct($name, $surname, $salary)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->salary = $salary;
    }

    // Геттер для имени:
    public function getName()
    {
        return $this->name;
    }

    // Геттер для фамилии:
    public function getSurname()
    {
        return $this->surname;
    }

    // Геттер для зарплаты:
    public function getSalary()
    {
        return $this->salary;
    }

    // Сеттер для зарплаты:
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }
}
