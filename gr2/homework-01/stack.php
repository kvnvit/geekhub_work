<?php

class Stack
{
    protected $stack;
    protected $element;
    protected $limit;
            
    public function __construct($limit = 10) //устанавливаем лимит стека по умолчанию
    { 
        $this->stack = array(); //инициализация стека         
        $this->limit = $limit; //cтек может содержать только это количество элементов 
    } 
        
    // Добавляем элемент на вершину стека
    public function stackPush($element)
    {
        if (count($this->stack) < $this->limit) {             
            array_push($this->stack, $element); // добавить элемент в конец массива 
        } else { 
            throw new RunTimeException('Стек переполнен!'); // выброс исключения при переполнении стека
        }    
    }
    
    // Удаляем элемент с вершины стека и возвращаем его. Если стек пустой - выбрасывает исключение
    public function stackPop()
    {
        if ($this->isEmpty()) {             
            new RunTimeException('Стек пуст!'); // выброс исключения при опустошения стека
        } else {             
            return array_pop($this->stack); // извлечь элемент из конца массива 
        }
    }
    
    // Какой элемент находится на вершине стека
    public function stackTop() 
    { 
        return end($this->stack);
    } 

    //  Проверка не пустой ли стек
    public function isEmpty() 
    { 
        return empty($this->stack);
    }       
}
    
?>