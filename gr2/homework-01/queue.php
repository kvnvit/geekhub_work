<?php

class Queue
{
    protected $queue;
    protected $element_q;
    protected $limit_q;

    public function __construct($limit_q = 10) //устанавливаем лимит очереди по умолчанию
    { 
        $this->queue = array(); //инициализация очереди         
        $this->limit_q = $limit_q; //очередь может содержать только это количество элементов 
    }         

    // Добавляем элемент в очередь
    public function Enqueue($element_q)
    {       
        if (count($this->queue) < $this->limit_q) {            
            array_push($this->queue, $element_q);  // добавить элемент в начало массива
        } else { 
            throw new RunTimeException('Очередь переполнена!'); // выброс исключения при переполнении очереди
        }    
    }
    
    // Удаляем первый помещенный элемент из очереди и возвращаем его. Если очередь пустая - выбрасывает исключение
    public function Dequeue()
    {   
        if ($this->isEmpty()) { 
            new RunTimeException('Очередь пуста!'); // выброс исключения при опустошения стека
        } else { 
            return array_shift($this->queue); // извлечь элемент из начала массива
        }
    }
    
    // Какой элемент находится в начале очереди
    public function Peek()
    {
        return current($this->queue);
    }
    
    //  Проверка не пустая ли очередь
    public function isEmpty() 
    { 
        return empty($this->queue);
    }    
}
    
?>