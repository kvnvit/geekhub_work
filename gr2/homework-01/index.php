<?php

// !!Задание: создать два класса для реализации стека и очереди

    require_once 'stack.php'; // подключаем класс стека
    require_once 'queue.php'; // подключаем класс очереди
    
// Добавим несколько элементов в стек
$element = new Stack(); 
$element->stackPush('1-й элемент стека'); 
$element->stackPush('2-й элемент стека'); 
$element->stackPush('3-й элемент стека'); 
$element->stackPush('4-й элемент стека'); 
$element->stackPush('5-й элемент стека'); 
$element->stackPush('6-й элемент стека'); 
$element->stackPush('7-й элемент стека'); 
$element->stackPush('8-й элемент стека'); 
// $element->stackPush('9-й элемент стека'); 
// $element->stackPush('10-й элемент стека'); 
// $element->stackPush('11-й элемент стека'); 

echo 'Добавим несколько элементов в стек:';
echo '<pre>';
print_r($element);
echo '</pre>';

echo 'Удалим некоторые элементы из вершины стека:'.'<br>';
echo $element->stackPop().'<br>'; // Удалим 8-й элемент стека 
echo $element->stackPop().'<br>'; // Удалим 7-й элемент стека 
echo $element->stackPop().'<br>'; // Удалим 6-й элемент стека 

echo '<br>'.'В стеке остались:';
echo '<pre>';
print_r($element);
echo '</pre>';

echo 'Посмотрим, что находится на вершине стека:'.'<br>';
echo $element->stackTop().'<br>'; // на вершине - 5-й элемент 

echo '<br>'.'Удалим и его:'.'<br>';
echo $element->stackPop().'<br>'; // Удалим 5-й элемент стека
echo '<pre>';
print_r($element);
echo '</pre>';

echo '<br>'.'Добавим еще один элемент в стек:';
$element->stackPush('новый элемент стека'); 
echo '<pre>';
print_r($element);
echo '</pre>';
echo '<hr>';

// Реализация очереди

// Добавим несколько элементов в очередь
$element_q = new Queue(); 
$element_q->Enqueue('1-й элемент очереди'); 
$element_q->Enqueue('2-й элемент очереди'); 
$element_q->Enqueue('3-й элемент очереди'); 
$element_q->Enqueue('4-й элемент очереди'); 
$element_q->Enqueue('5-й элемент очереди'); 
$element_q->Enqueue('6-й элемент очереди'); 
$element_q->Enqueue('7-й элемент очереди'); 
$element_q->Enqueue('8-й элемент очереди'); 

echo 'Добавим несколько элементов в очередь:';
echo '<pre>';
print_r($element_q);
echo '</pre>';

echo 'Удалим некоторые элементы из начала очереди:'.'<br>';
echo $element_q->Dequeue().'<br>'; // Удалим 8-й элемент очереди 
echo $element_q->Dequeue().'<br>'; // Удалим 7-й элемент очереди 
echo $element_q->Dequeue().'<br>'; // Удалим 6-й элемент очереди 

echo '<br>'.'В очереди остались:';
echo '<pre>';
print_r($element_q);
echo '</pre>';

echo 'Посмотрим, что находится в начале очереди:'.'<br>';
echo $element_q->Peek().'<br>'; // в начале - 4-й элемент 

echo '<br>'.'Удалим и его:'.'<br>';
echo $element_q->Dequeue().'<br>'; // Удалим 4-й элемент очереди
echo '<pre>';
print_r($element_q);
echo '</pre>';

echo '<br>'.'Добавим еще один элемент в очередь:';
$element_q->Enqueue('новый элемент очереди'); 
echo '<pre>';
print_r($element_q);
echo '</pre>';

?>