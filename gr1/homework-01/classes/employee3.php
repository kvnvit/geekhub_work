<?php

// Класс для главы 4 (11)
class Employee3
{
    public $name;
    public $salary;

    // Метод для изменения зарплаты:
    public function doubleSalary($salary)
    {
        $this->salary = $salary * 2;
    }
}
